import java.util.ArrayList;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        //Forma de declarar variables en java
        // instancia de persona

        Persona sebas = new Persona();
        sebas.name = "Richard Rosero ";
        sebas.edad = 20;
        sebas.active = true;
        sebas.height = 1.76;
        sebas.fechaDeNacimiento = new Date();
        sebas.type = "bombero";

        Persona carito = new Persona();
        sebas.name = "carolina ordoñez ";
        sebas.edad = 20;
        sebas.active = true;
        sebas.height = 1.63;
        sebas.fechaDeNacimiento = new Date();
        sebas.type = "Deportista";

        // instancia de visita medica

        VisitaMedica nuevaVisita = new VisitaMedica();
        nuevaVisita.diagnostico = " cefalea ";
        nuevaVisita.sintomas = " dolor de cabeza ";
        nuevaVisita.fecha = new Date();

        // instamciar la historia clinica de un paciente

        HistoriaClinica nuevaHistoria = new HistoriaClinica();
        nuevaHistoria.eps = "sanitas";
        nuevaHistoria.ips = "Hospital departamental";
        nuevaHistoria.persona = sebas;
        nuevaHistoria.VisitaMedicas = new ArrayList<>();
        nuevaHistoria.VisitaMedicas.add(nuevaVisita);

        Profesor profesor= new Profesor();
        profesor.trabajar();

        Deportista deportista = new Deportista();
        deportista.trabajar();

        Bombero bombero = new Bombero();
        bombero.trabajar();

        Persona bombero1 = new Bombero();
        bombero1.trabajar();


        // impresiones

        System.out.println(sebas.name);
        System.out.println(sebas.edad);
        System.out.println(sebas.active);
        System.out.println(sebas.height);
        System.out.println(sebas.trabajar());
        System.out.println(profesor.trabajar());
        System.out.println(bombero.trabajar());
        System.out.println(deportista.trabajar());
        System.out.println(bombero1.trabajar());
        System.out.println(sebas.fechaDeNacimiento);
        System.out.println(nuevaHistoria.eps);
        System.out.println(nuevaHistoria.ips);
        System.out.println(nuevaVisita.diagnostico);
        System.out.println(nuevaVisita.sintomas);
        System.out.println(nuevaVisita.fecha);


    }
}
